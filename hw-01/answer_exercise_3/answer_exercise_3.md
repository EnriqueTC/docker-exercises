Para este ejercicio como no se especifica el uso de Dockerfile explicaré una versión con y otra sin:

1- Versión sin Dockerfile:
	
	* El primer paso es crear el docker:
		
		docker run -d --name exercise3 -p 8080:80 -v static_content:/demo nginx:1.19.3

		* -d -> modo deamon.
		* --name exercise3 -> nombre del docker.
		* -p 8080:80 -> el puerto 8080 para poder acceder en localhost.
		* -v static_content:/demo -> crea el volumen static_content si no existe, y dentro del docker estará especificado en la carpeta demo.
		* nginx:1.19.3 -> setea la imagen base con el tag especificado.

	* El segundo paso es modificar el fichero index.html:
		
		Primero tenemos que entrar en el docker usando el comando docker exec -it exercise3 sh.
		
		Una vez dentro, el fichero se encuentra en la ruta /usr/share/nginx/html/ y para poder editarlo necesitamos tener un editor instalado. 
		Para ello debemos hacer un apt update y posteriormente un apt install vim una vez insataldo ya se puedo modificar el fichero index.html usando el comando vim index.html.

	* Y el último paso es copiar el fichero al volumen:
	
		Para ello se usrá el comando  cp /usr/share/nginx/html/index.html /demo/ dónde /demo es la carpeta que hace referencia al volumen.		


2- Versión con Dockerfile:

	* Para esta versión se tiene que generar un index.html (como el de la carpeta actual) y crear un Dockerfile con las siguientes instrucciones:
		
		* FROM nginx:1.19.3 -> setea la imagen base con el tag especificado.

		* VOLUME static_content:/demo -> crea el volumen static_content si no existe, y dentro del docker estará especificado en la carpeta demo.

		* COPY /index.html /usr/share/nginx/html/ -> sobreescribe el archivo index.html para mostrar el mensaje deseado.
		
	* El siguiente paso es construir la imagen a partir del Dockerfile.

		docker build -t exercise3image . 
	
	* Una vez construida la imagen, ejecutaremos el docker.
		
		docker run -d --name exercise3 -p 8080:80 exercise3image 

	* Por último nos falta copiar el fichero index.html en el volumen static_content.
	
		docker cp index.html 0978a2cd52d3:/demo (0978a2cd52d3 id del docker, se puede consultar con docker ps)

		+Info: el fichero también se puede copiar como la explicación anterior.
		+Info: no he usado la instrucción COPY index.html /demo porque si por alguna razón se modifca el fichero index.html en el volumen, al construir otro docker se reemplazaría por el primer index.html y se perderian los cambios hechos en el index.html del volumen.

	(Tanto el Dockerfile como el index.html estan definidos en la carpeta del ejercicio)


Para concluir, las dos opciones són válidas aunque la segunda es mucho mejor ya que es más limpia y más fácil de construir el docker y mucho más rápido.