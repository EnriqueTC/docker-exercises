En este ejercicio se nos pide utilizar docker-compose para levantar un ElasticSearch y Kibana con unas ciertas características.

Esta carpeta contiene el fichero docker-compose.yml.

Para ejecutar docker compose se usa el siguiente comando:

docker-compose up 

Una vez arrancado ya funciona el ejemplo del enunciado.