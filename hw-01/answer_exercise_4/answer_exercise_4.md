Para este ejercicio se hace uso del Dockerfile de la carpeta actual y se ejecutan las siguientes instrucciones:
	
	* Primero construimos la imagen a partir del Dockerfile.

		docker build -t exercise4image . 
	
	* Una vez construida la imagen, ejecutaremos el docker.
		
		docker run -d --name exercise4 -p 8080:8080 exercise4image 

	* Por último esperamos a que deje de mostrar starting al hacer docker ps y nos mostrará healthy.